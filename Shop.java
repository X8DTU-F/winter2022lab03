import java.util.Scanner;

public class Shop {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Computer[] list = new Computer[4];
        for (int x=0; x < list.length; x++) {
            list[x] = new Computer();

            System.out.println("Which model do you have?");
            list[x].model = sc.next();
            System.out.println("How much did it cost?");
            list[x].price = sc.nextInt();
            System.out.println("How fast is it?");
            list[x].performance = sc.next();
        }

        System.out.println(list[3].model);
        System.out.println(list[3].price);
        System.out.println(list[3].performance);

        list[3].howFast();
        System.out.println(list[3].performance);
    }
}
